Modules extend your site functionality beyond Vertcms core.

WHAT TO PLACE IN THIS DIRECTORY?
--------------------------------

Placing downloaded and custom modules in this directory separates downloaded and
custom modules from Vertcms core's modules. This allows Vertcms core to be updated
without overwriting these files.

DOWNLOAD ADDITIONAL MODULES
---------------------------

Contributed modules from the Vertcms community may be downloaded at
https://www.vertcms.org/project/project_module.

ORGANIZING MODULES IN THIS DIRECTORY
------------------------------------

You may create subdirectories in this directory, to organize your added modules,
without breaking the site. Some common subdirectories include "contrib" for
contributed modules, and "custom" for custom modules. Note that if you move a
module to a subdirectory after it has been enabled, you may need to clear the
Vertcms cache so it can be found. (Alternatively, you can disable the module
before moving it and then re-enable it after the move.)

MULTISITE CONFIGURATION
-----------------------

In multisite configurations, modules found in this directory are available to
all sites. Alternatively, the sites/your_site_name/modules directory pattern
may be used to restrict modules to a specific site instance.

MORE INFORMATION
----------------

Refer to the "Developing for Vertcms" section of the README.txt in the Vertcms
root directory for further information on extending Vertcms with custom modules.
