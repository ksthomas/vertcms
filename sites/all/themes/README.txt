Themes allow you to change the look and feel of your Vertcms site. You can use
themes contributed by others or create your own.

WHAT TO PLACE IN THIS DIRECTORY?
--------------------------------

Placing downloaded and custom themes in this directory separates downloaded and
custom themes from Vertcms core's themes. This allows Vertcms core to be updated
without overwriting these files.

DOWNLOAD ADDITIONAL THEMES
--------------------------

Contributed themes from the Vertcms community may be downloaded at
https://www.vertcms.org/project/project_theme.

MULTISITE CONFIGURATION
-----------------------

In multisite configurations, themes found in this directory are available to
all sites. Alternatively, the sites/your_site_name/themes directory pattern
may be used to restrict themes to a specific site instance.

MORE INFORMATION
-----------------

Refer to the "Appearance" section of the README.txt in the Vertcms root directory
for further information on customizing the appearance of Vertcms with custom
themes.
