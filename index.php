<?php

/**
 * @file
 * The PHP page that serves all page requests on a Vertcms installation.
 *
 * The routines here dispatch control to the appropriate handler, which then
 * prints the appropriate page.
 *
 * All Vertcms code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 */

/**
 * Root directory of Vertcms installation.
 */
define('VERTCMS_ROOT', getcwd());

require_once VERTCMS_ROOT . '/zengine/includes/bootstrap.inc';
vertcms_bootstrap(VERTCMS_BOOTSTRAP_FULL);
menu_execute_active_handler();
