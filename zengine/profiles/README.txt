Installation profiles define additional steps that run after the base
installation provided by Vertcms core when Vertcms is first installed.

WHAT TO PLACE IN THIS DIRECTORY?
--------------------------------

Place downloaded and custom installation profiles in this directory.
Installation profiles are generally provided as part of a Vertcms distribution.
They only impact the installation of your site. They do not have any effect on
an already running site.

DOWNLOAD ADDITIONAL DISTRIBUTIONS
---------------------------------

Contributed distributions from the Vertcms community may be downloaded at
https://www.vertcms.org/project/project_distribution.

MULTISITE CONFIGURATION
-----------------------

In multisite configurations, installation profiles found in this directory are
available to all sites during their initial site installation.

MORE INFORMATION
----------------

Refer to the "Installation profiles" section of the README.txt in the Vertcms
root directory for further information on extending Vertcms with custom profiles.
