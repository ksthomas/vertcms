<?php
global $user;

?>
<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">

    <!--<input class="form-control form-control-dark w-25" type="text" placeholder="Search" aria-label="Search">-->

    <div class="w-100"><a class="navbar-brand col-auto mr-0" href="/">Vertiqly</a>

        <!--<button id="api-modules" title="Modules / Extensions"><i class="fas fa-coins fa-2x"></i></button>





        <button id="api-users" title="Users"><i class="fas fa-user fa-2x"></i></button>-->

<span style="float:right;">
    <button id="api-settings" title="Main System Settings"><i class="fas fa-cog fa-2x"></i></button>
    <button id="api-cc" title="Clear All Caches"><i class="fas fa-trash-alt fa-2x"></i></button>
  <?php if($user->uid == 0){ ?>
        <button id="api-sign-in" title="Sign In"><i class="fas fa-power-off fa-2x"></i></button>
  <?php }else{ ?>
        <button id="api-sign-out" title="Sign Out"><i class="fas fa-power-off fa-2x"></i></button>
  <?php } ?>
</span>
    </div>

    <!--
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="/user/logout">Sign Out</a>
        </li>
    </ul>
    -->

</nav>
<div class="container-fluid">
    <div class="row">
        <!--
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">

              <?php if ($page['sidebar_first']): ?>
                      <?php //print render($page['sidebar_first']); ?>
              <?php endif; ?>

            </div>
        </nav>
-->
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link active" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                            Dashboard <span class="sr-only">(current)</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a id="api-content" class="nav-link" href="/node/add/issue">
                            <i class="far fa-arrow-alt-circle-up fa-1x"></i> New Issue
                        </a>
                    </li>
                    <li class="nav-item">
                        <a id="api-content" class="nav-link" href="/node/add/article">
                            <i class="far fa-arrow-alt-circle-up fa-1x"></i> New Article
                        </a>
                    </li>
                    <li class="nav-item">
                        <a id="api-content" class="nav-link" href="/admin/content">
                            <i class="fas fa-edit fa-1x"></i> Browse Content
                        </a>
                    </li>


                    <li class="nav-item">
                        <a id="api-users" class="nav-link" href="#">
                            <i class="fas fa-user fa-1x"></i> People
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
        <main role="main" class="col-md-10 ml-sm-auto col-lg-10 px-4">

            <div class="w-100 modal" role="dialog"  id="xvajax-message"></div>

            <div class="modal fade" id="vajax-message" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">System Message</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div id="vajax-message-body" class="modal-body">

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="vajax-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="vajax-modal-title"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div id="vajax-modal-body" class="modal-body">

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>



            <div id="branding" class="clearfix">
              <?php print $breadcrumb; ?>
              <?php print render($title_prefix); ?>
              <?php if ($title): ?>
                  <h1 class="page-title"><?php print $title; ?></h1>
              <?php endif; ?>
              <?php print render($title_suffix); ?>
              <?php //print render($primary_local_tasks); ?>
            </div>

            <div id="page">
              <?php if ($secondary_local_tasks): ?>
                  <div class="tabs-secondary clearfix"><?php //print render($secondary_local_tasks); ?></div>
              <?php endif; ?>

                <div id="content" class="clearfix">
                    <div class="element-invisible"><a id="main-content"></a></div>
                  <?php if ($messages): ?>
                      <div id="console" class="clearfix"><?php print $messages; ?></div>
                  <?php endif; ?>
                  <?php if ($page['help']): ?>
                      <div id="help">
                        <?php print render($page['help']); ?>
                      </div>
                  <?php endif; ?>
                  <?php $action_links=null; if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
                  <?php print render($page['content']); ?>
                </div>

                <div id="footer">
                  <?php print $feed_icons; ?>
                </div>

            </div>




        </main>
    </div>
</div>
<script>
  $( "#api-cc" ).click(function() {
  $.get( "/api/cc", function( data ) {
    //$( ".btncc" ).html( data );
    //alert(data);
    $( "#api-cc" ).html('<i style="color:green;" class="far fa-trash-alt fa-2x"></i>').slideUp(400).fadeIn( 400 );
    $("#vajax-message-body").html(data).fadeIn(500);
    $("#vajax-message").modal('show');
    //$( "#api-cc" ).html('<i style="color:green;" class="far fa-trash-alt fa-2x"></i>').effect( "shake" );

  });
  });

  $( "#api-settings" ).click(function() {
    window.location.href = '/admin/index';
    //$.get( "/admin", function( data ) {
        //$("#page").html(data);
      //$("#vajax-message-body").html(data).fadeIn(500);
      //$("#vajax-message").modal('show');

    //});
  });

  $( "#api-modules" ).click(function() {
    window.location.href = '/admin/modules';
  });

  $( "#api-sign-in" ).click(function() {
    window.location.href = '/user/login';
    /*
    $.get( "/user/login", function( data ) {
    $("#vajax-modal-body").html(data).fadeIn(500);
    $("#vajax-modal").modal('show');
    $("#vajax-modal-title").html('Login');
    });
    */

  });

  $( "#api-sign-out" ).click(function() {
    window.location.href = '/user/logout';
  });

  $( "#api-users" ).click(function() {
    window.location.href = '/admin/people';
  });

  $( "#api-content" ).click(function() {
    window.location.href = '/admin/content';
  });

</script>

<script>
  ClassicEditor
      .create( document.querySelector( '#edit-body-und-0-value' ) )
      .catch( error => {
    console.error( error );
  } );
</script>