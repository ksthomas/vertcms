(function ($) {

Vertcms.behaviors.contentTypes = {
  attach: function (context) {
    // Provide the vertical tab summaries.
    $('fieldset#edit-submission', context).vertcmsSetSummary(function(context) {
      var vals = [];
      vals.push(Vertcms.checkPlain($('#edit-title-label', context).val()) || Vertcms.t('Requires a title'));
      return vals.join(', ');
    });
    $('fieldset#edit-workflow', context).vertcmsSetSummary(function(context) {
      var vals = [];
      $("input[name^='node_options']:checked", context).parent().each(function() {
        vals.push(Vertcms.checkPlain($(this).text()));
      });
      if (!$('#edit-node-options-status', context).is(':checked')) {
        vals.unshift(Vertcms.t('Not published'));
      }
      return vals.join(', ');
    });
    $('fieldset#edit-display', context).vertcmsSetSummary(function(context) {
      var vals = [];
      $('input:checked', context).next('label').each(function() {
        vals.push(Vertcms.checkPlain($(this).text()));
      });
      if (!$('#edit-node-submitted', context).is(':checked')) {
        vals.unshift(Vertcms.t("Don't display post information"));
      }
      return vals.join(', ');
    });
  }
};

})(jQuery);
