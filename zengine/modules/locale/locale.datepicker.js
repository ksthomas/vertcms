(function ($) {

/**
 * Attaches language support to the jQuery UI datepicker component.
 */
Vertcms.behaviors.localeDatepicker = {
  attach: function(context, settings) {
    // This code accesses Vertcms.settings and localized strings via Vertcms.t().
    // So this code should run after these are initialized. By placing it in an
    // attach behavior this is assured.
    $.datepicker.regional['vertcms-locale'] = $.extend({
      closeText: Vertcms.t('Done'),
      prevText: Vertcms.t('Prev'),
      nextText: Vertcms.t('Next'),
      currentText: Vertcms.t('Today'),
      monthNames: [
        Vertcms.t('January'),
        Vertcms.t('February'),
        Vertcms.t('March'),
        Vertcms.t('April'),
        Vertcms.t('May'),
        Vertcms.t('June'),
        Vertcms.t('July'),
        Vertcms.t('August'),
        Vertcms.t('September'),
        Vertcms.t('October'),
        Vertcms.t('November'),
        Vertcms.t('December')
      ],
      monthNamesShort: [
        Vertcms.t('Jan'),
        Vertcms.t('Feb'),
        Vertcms.t('Mar'),
        Vertcms.t('Apr'),
        Vertcms.t('May'),
        Vertcms.t('Jun'),
        Vertcms.t('Jul'),
        Vertcms.t('Aug'),
        Vertcms.t('Sep'),
        Vertcms.t('Oct'),
        Vertcms.t('Nov'),
        Vertcms.t('Dec')
      ],
      dayNames: [
        Vertcms.t('Sunday'),
        Vertcms.t('Monday'),
        Vertcms.t('Tuesday'),
        Vertcms.t('Wednesday'),
        Vertcms.t('Thursday'),
        Vertcms.t('Friday'),
        Vertcms.t('Saturday')
      ],
      dayNamesShort: [
        Vertcms.t('Sun'),
        Vertcms.t('Mon'),
        Vertcms.t('Tue'),
        Vertcms.t('Wed'),
        Vertcms.t('Thu'),
        Vertcms.t('Fri'),
        Vertcms.t('Sat')
      ],
      dayNamesMin: [
        Vertcms.t('Su'),
        Vertcms.t('Mo'),
        Vertcms.t('Tu'),
        Vertcms.t('We'),
        Vertcms.t('Th'),
        Vertcms.t('Fr'),
        Vertcms.t('Sa')
      ],
      dateFormat: Vertcms.t('mm/dd/yy'),
      firstDay: 0,
      isRTL: 0
    }, Vertcms.settings.jquery.ui.datepicker);
    $.datepicker.setDefaults($.datepicker.regional['vertcms-locale']);
  }
};

})(jQuery);
