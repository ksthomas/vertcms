
Vertcms.t("Standard Call t");
Vertcms
.
t
(
"Whitespace Call t"
)
;

Vertcms.t('Single Quote t');
Vertcms.t('Single Quote \'Escaped\' t');
Vertcms.t('Single Quote ' + 'Concat ' + 'strings ' + 't');

Vertcms.t("Double Quote t");
Vertcms.t("Double Quote \"Escaped\" t");
Vertcms.t("Double Quote " + "Concat " + "strings " + "t");

Vertcms.t("Context Unquoted t", {}, {context: "Context string unquoted"});
Vertcms.t("Context Single Quoted t", {}, {'context': "Context string single quoted"});
Vertcms.t("Context Double Quoted t", {}, {"context": "Context string double quoted"});

Vertcms.t("Context !key Args t", {'!key': 'value'}, {context: "Context string"});

Vertcms.formatPlural(1, "Standard Call plural", "Standard Call @count plural");
Vertcms
.
formatPlural
(
1,
"Whitespace Call plural",
"Whitespace Call @count plural"
)
;

Vertcms.formatPlural(1, 'Single Quote plural', 'Single Quote @count plural');
Vertcms.formatPlural(1, 'Single Quote \'Escaped\' plural', 'Single Quote \'Escaped\' @count plural');

Vertcms.formatPlural(1, "Double Quote plural", "Double Quote @count plural");
Vertcms.formatPlural(1, "Double Quote \"Escaped\" plural", "Double Quote \"Escaped\" @count plural");

Vertcms.formatPlural(1, "Context Unquoted plural", "Context Unquoted @count plural", {}, {context: "Context string unquoted"});
Vertcms.formatPlural(1, "Context Single Quoted plural", "Context Single Quoted @count plural", {}, {'context': "Context string single quoted"});
Vertcms.formatPlural(1, "Context Double Quoted plural", "Context Double Quoted @count plural", {}, {"context": "Context string double quoted"});

Vertcms.formatPlural(1, "Context !key Args plural", "Context !key Args @count plural", {'!key': 'value'}, {context: "Context string"});
