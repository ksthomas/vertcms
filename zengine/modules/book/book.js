/**
 * @file
 * Javascript behaviors for the Book module.
 */

(function ($) {

Vertcms.behaviors.bookFieldsetSummaries = {
  attach: function (context) {
    $('fieldset.book-outline-form', context).vertcmsSetSummary(function (context) {
      var $select = $('.form-item-book-bid select');
      var val = $select.val();

      if (val === '0') {
        return Vertcms.t('Not in book');
      }
      else if (val === 'new') {
        return Vertcms.t('New book');
      }
      else {
        return Vertcms.checkPlain($select.find(':selected').text());
      }
    });
  }
};

})(jQuery);
