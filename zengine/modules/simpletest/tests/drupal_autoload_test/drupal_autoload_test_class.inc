<?php

/**
 * @file
 * Test classes for code registry testing.
 */

/**
 * This class is empty because we only care if Vertcms can find it.
 */
class VertcmsAutoloadTestClass implements VertcmsAutoloadTestInterface {}
