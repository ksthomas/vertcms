<?php

/**
 * @file
 * Helper module for the Common tests.
 */

/**
 * Implements hook_menu().
 */
function common_test_menu() {
  $items['common-test/vertcms_goto'] = array(
    'title' => 'Vertcms Goto',
    'page callback' => 'common_test_vertcms_goto_land',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['common-test/vertcms_goto/fail'] = array(
    'title' => 'Vertcms Goto',
    'page callback' => 'common_test_vertcms_goto_land_fail',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['common-test/vertcms_goto/redirect'] = array(
    'title' => 'Vertcms Goto',
    'page callback' => 'common_test_vertcms_goto_redirect',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['common-test/vertcms_goto/redirect_advanced'] = array(
    'title' => 'Vertcms Goto',
    'page callback' => 'common_test_vertcms_goto_redirect_advanced',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['common-test/vertcms_goto/redirect_fail'] = array(
    'title' => 'Vertcms Goto Failure',
    'page callback' => 'vertcms_goto',
    'page arguments' => array('common-test/vertcms_goto/fail'),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['common-test/destination'] = array(
    'title' => 'Vertcms Get Destination',
    'page callback' => 'common_test_destination',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['common-test/query-string'] = array(
    'title' => 'Test querystring',
    'page callback' => 'common_test_js_and_css_querystring',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Redirect using vertcms_goto().
 */
function common_test_vertcms_goto_redirect() {
  vertcms_goto('common-test/vertcms_goto');
}

/**
 * Redirect using vertcms_goto().
 */
function common_test_vertcms_goto_redirect_advanced() {
  vertcms_goto('common-test/vertcms_goto', array('query' => array('foo' => '123')), 301);
}

/**
 * Landing page for vertcms_goto().
 */
function common_test_vertcms_goto_land() {
  print "vertcms_goto";
}

/**
 * Fail landing page for vertcms_goto().
 */
function common_test_vertcms_goto_land_fail() {
  print "vertcms_goto_fail";
}

/**
 * Implements hook_vertcms_goto_alter().
 */
function common_test_vertcms_goto_alter(&$path, &$options, &$http_response_code) {
  if ($path == 'common-test/vertcms_goto/fail') {
    $path = 'common-test/vertcms_goto/redirect';
  }
}

/**
 * Implements hook_init().
 */
function common_test_init() {
  if (variable_get('common_test_redirect_current_path', FALSE)) {
    vertcms_goto(current_path());
  }
  if (variable_get('common_test_link_to_current_path', FALSE)) {
    vertcms_set_message(l('link which should point to the current path', current_path()));
  }
}

/**
 * Print destination query parameter.
 */
function common_test_destination() {
  $destination = vertcms_get_destination();
  print "The destination: " . check_plain($destination['destination']);
}

/**
 * Applies #printed to an element to help test #pre_render.
 */
function common_test_vertcms_render_printing_pre_render($elements) {
  $elements['#printed'] = TRUE;
  return $elements;
}

/**
 * Implements hook_TYPE_alter().
 */
function common_test_vertcms_alter_alter(&$data, &$arg2 = NULL, &$arg3 = NULL) {
  // Alter first argument.
  if (is_array($data)) {
    $data['foo'] = 'Vertcms';
  }
  elseif (is_object($data)) {
    $data->foo = 'Vertcms';
  }
  // Alter second argument, if present.
  if (isset($arg2)) {
    if (is_array($arg2)) {
      $arg2['foo'] = 'Vertcms';
    }
    elseif (is_object($arg2)) {
      $arg2->foo = 'Vertcms';
    }
  }
  // Try to alter third argument, if present.
  if (isset($arg3)) {
    if (is_array($arg3)) {
      $arg3['foo'] = 'Vertcms';
    }
    elseif (is_object($arg3)) {
      $arg3->foo = 'Vertcms';
    }
  }
}

/**
 * Implements hook_TYPE_alter() on behalf of Bartik theme.
 *
 * Same as common_test_vertcms_alter_alter(), but here, we verify that themes
 * can also alter and come last.
 */
function bartik_vertcms_alter_alter(&$data, &$arg2 = NULL, &$arg3 = NULL) {
  // Alter first argument.
  if (is_array($data)) {
    $data['foo'] .= ' theme';
  }
  elseif (is_object($data)) {
    $data->foo .= ' theme';
  }
  // Alter second argument, if present.
  if (isset($arg2)) {
    if (is_array($arg2)) {
      $arg2['foo'] .= ' theme';
    }
    elseif (is_object($arg2)) {
      $arg2->foo .= ' theme';
    }
  }
  // Try to alter third argument, if present.
  if (isset($arg3)) {
    if (is_array($arg3)) {
      $arg3['foo'] .= ' theme';
    }
    elseif (is_object($arg3)) {
      $arg3->foo .= ' theme';
    }
  }
}

/**
 * Implements hook_TYPE_alter() on behalf of block module.
 *
 * This is for verifying that vertcms_alter(array(TYPE1, TYPE2), ...) allows
 * hook_module_implements_alter() to affect the order in which module
 * implementations are executed.
 */
function block_vertcms_alter_foo_alter(&$data, &$arg2 = NULL, &$arg3 = NULL) {
  $data['foo'] .= ' block';
}

/**
 * Implements hook_module_implements_alter().
 *
 * @see block_vertcms_alter_foo_alter()
 */
function common_test_module_implements_alter(&$implementations, $hook) {
  // For vertcms_alter(array('vertcms_alter', 'vertcms_alter_foo'), ...), make the
  // block module implementations run after all the other modules. Note that
  // when vertcms_alter() is called with an array of types, the first type is
  // considered primary and controls the module order.
  if ($hook == 'vertcms_alter_alter' && isset($implementations['block'])) {
    $group = $implementations['block'];
    unset($implementations['block']);
    $implementations['block'] = $group;
  }
}

/**
 * Implements hook_theme().
 */
function common_test_theme() {
  return array(
    'common_test_foo' => array(
      'variables' => array('foo' => 'foo', 'bar' => 'bar'),
    ),
  );
}

/**
 * Theme function for testing vertcms_render() theming.
 */
function theme_common_test_foo($variables) {
  return $variables['foo'] . $variables['bar'];
}

/**
 * Implements hook_library_alter().
 */
function common_test_library_alter(&$libraries, $module) {
  if ($module == 'system' && isset($libraries['farbtastic'])) {
    // Change the title of Farbtastic to "Farbtastic: Altered Library".
    $libraries['farbtastic']['title'] = 'Farbtastic: Altered Library';
    // Make Farbtastic depend on jQuery Form to test library dependencies.
    $libraries['farbtastic']['dependencies'][] = array('system', 'form');
  }
}

/**
 * Implements hook_library().
 *
 * Adds Farbtastic in a different version.
 */
function common_test_library() {
  $libraries['farbtastic'] = array(
    'title' => 'Custom Farbtastic Library',
    'website' => 'http://code.google.com/p/farbtastic/',
    'version' => '5.3',
    'js' => array(
      'misc/farbtastic/farbtastic.js' => array(),
    ),
    'css' => array(
      'zengine/misc/farbtastic/farbtastic.css' => array(),
    ),
  );
  return $libraries;
}

/**
 * Adds a JavaScript file and a CSS file with a query string appended.
 */
function common_test_js_and_css_querystring() {
   vertcms_add_js(vertcms_get_path('module', 'node') . '/node.js');
   vertcms_add_css(vertcms_get_path('module', 'node') . '/node.css');
   // A relative URI may have a query string.
   vertcms_add_css('/' . vertcms_get_path('module', 'node') . '/node-fake.css?arg1=value1&arg2=value2');
   return '';
}

/**
 * Implements hook_cron().
 *
 * System module should handle if a module does not catch an exception and keep
 * cron going.
 *
 * @see common_test_cron_helper()
 *
 */
function common_test_cron() {
  throw new Exception(t('Uncaught exception'));
}
