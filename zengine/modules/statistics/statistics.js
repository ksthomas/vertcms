(function ($) {
  $(document).ready(function() {
    $.ajax({
      type: "POST",
      cache: false,
      url: Vertcms.settings.statistics.url,
      data: Vertcms.settings.statistics.data
    });
  });
})(jQuery);
