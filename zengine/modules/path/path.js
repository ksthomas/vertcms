
/**
 * @file
 * Attaches behaviors for the Path module.
 */

(function ($) {

Vertcms.behaviors.pathFieldsetSummaries = {
  attach: function (context) {
    $('fieldset.path-form', context).vertcmsSetSummary(function (context) {
      var path = $('.form-item-path-alias input').val();

      return path ?
        Vertcms.t('Alias: @alias', { '@alias': path }) :
        Vertcms.t('No alias');
    });
  }
};

})(jQuery);
