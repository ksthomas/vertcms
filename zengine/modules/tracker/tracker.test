<?php

/**
 * @file
 * Tests for tracker.module.
 */

/**
 * Defines a base class for testing tracker.module.
 */
class TrackerTest extends VertcmsWebTestCase {

  /**
   * The main user for testing.
   *
   * @var object
   */
  protected $user;

  /**
   * A second user that will 'create' comments and nodes.
   *
   * @var object
   */
  protected $other_user;

  public static function getInfo() {
    return array(
      'name' => 'Tracker',
      'description' => 'Create and delete nodes and check for their display in the tracker listings.',
      'group' => 'Tracker'
    );
  }

  function setUp() {
    parent::setUp('comment', 'tracker');

    $permissions = array('access comments', 'create page content', 'post comments', 'skip comment approval');
    $this->user = $this->vertcmsCreateUser($permissions);
    $this->other_user = $this->vertcmsCreateUser($permissions);

    // Make node preview optional.
    variable_set('comment_preview_page', 0);
  }

  /**
   * Tests for the presence of nodes on the global tracker listing.
   */
  function testTrackerAll() {
    $this->vertcmsLogin($this->user);

    $unpublished = $this->vertcmsCreateNode(array(
      'title' => $this->randomName(8),
      'status' => 0,
    ));
    $published = $this->vertcmsCreateNode(array(
      'title' => $this->randomName(8),
      'status' => 1,
    ));

    $this->vertcmsGet('tracker');
    $this->assertNoText($unpublished->title, 'Unpublished node do not show up in the tracker listing.');
    $this->assertText($published->title, 'Published node show up in the tracker listing.');
    $this->assertLink(t('My recent content'), 0, 'User tab shows up on the global tracker page.');

    // Delete a node and ensure it no longer appears on the tracker.
    node_delete($published->nid);
    $this->vertcmsGet('tracker');
    $this->assertNoText($published->title, 'Deleted node do not show up in the tracker listing.');
  }

  /**
   * Tests for the presence of nodes on a user's tracker listing.
   */
  function testTrackerUser() {
    $this->vertcmsLogin($this->user);

    $unpublished = $this->vertcmsCreateNode(array(
      'title' => $this->randomName(8),
      'uid' => $this->user->uid,
      'status' => 0,
    ));
    $my_published = $this->vertcmsCreateNode(array(
      'title' => $this->randomName(8),
      'uid' => $this->user->uid,
      'status' => 1,
    ));
    $other_published_no_comment = $this->vertcmsCreateNode(array(
      'title' => $this->randomName(8),
      'uid' => $this->other_user->uid,
      'status' => 1,
    ));
    $other_published_my_comment = $this->vertcmsCreateNode(array(
      'title' => $this->randomName(8),
      'uid' => $this->other_user->uid,
      'status' => 1,
    ));
    $comment = array(
      'subject' => $this->randomName(),
      'comment_body[' . LANGUAGE_NONE . '][0][value]' => $this->randomName(20),
    );
    $this->vertcmsPost('comment/reply/' . $other_published_my_comment->nid, $comment, t('Save'));

    $this->vertcmsGet('user/' . $this->user->uid . '/track');
    $this->assertNoText($unpublished->title, "Unpublished nodes do not show up in the users's tracker listing.");
    $this->assertText($my_published->title, "Published nodes show up in the user's tracker listing.");
    $this->assertNoText($other_published_no_comment->title, "Other user's nodes do not show up in the user's tracker listing.");
    $this->assertText($other_published_my_comment->title, "Nodes that the user has commented on appear in the user's tracker listing.");

    // Verify that unpublished comments are removed from the tracker.
    $admin_user = $this->vertcmsCreateUser(array('administer comments', 'access user profiles'));
    $this->vertcmsLogin($admin_user);
    $this->vertcmsPost('comment/1/edit', array('status' => COMMENT_NOT_PUBLISHED), t('Save'));
    $this->vertcmsGet('user/' . $this->user->uid . '/track');
    $this->assertNoText($other_published_my_comment->title, 'Unpublished comments are not counted on the tracker listing.');
  }

  /**
   * Tests for the presence of the "new" flag for nodes.
   */
  function testTrackerNewNodes() {
    $this->vertcmsLogin($this->user);

    $edit = array(
      'title' => $this->randomName(8),
    );

    $node = $this->vertcmsCreateNode($edit);
    $title = $edit['title'];
    $this->vertcmsGet('tracker');
    $this->assertPattern('/' . $title . '.*new/', 'New nodes are flagged as such in the tracker listing.');

    $this->vertcmsGet('node/' . $node->nid);
    $this->vertcmsGet('tracker');
    $this->assertNoPattern('/' . $title . '.*new/', 'Visited nodes are not flagged as new.');

    $this->vertcmsLogin($this->other_user);
    $this->vertcmsGet('tracker');
    $this->assertPattern('/' . $title . '.*new/', 'For another user, new nodes are flagged as such in the tracker listing.');

    $this->vertcmsGet('node/' . $node->nid);
    $this->vertcmsGet('tracker');
    $this->assertNoPattern('/' . $title . '.*new/', 'For another user, visited nodes are not flagged as new.');
  }

  /**
   * Tests for comment counters on the tracker listing.
   */
  function testTrackerNewComments() {
    $this->vertcmsLogin($this->user);

    $node = $this->vertcmsCreateNode(array(
      'comment' => 2,
    ));

    // Add a comment to the page.
    $comment = array(
      'subject' => $this->randomName(),
      'comment_body[' . LANGUAGE_NONE . '][0][value]' => $this->randomName(20),
    );
    // The new comment is automatically viewed by the current user.
    $this->vertcmsPost('comment/reply/' . $node->nid, $comment, t('Save'));

    $this->vertcmsLogin($this->other_user);
    $this->vertcmsGet('tracker');
    $this->assertText('1 new', 'New comments are counted on the tracker listing pages.');
    $this->vertcmsGet('node/' . $node->nid);

    // Add another comment as other_user.
    $comment = array(
      'subject' => $this->randomName(),
      'comment_body[' . LANGUAGE_NONE . '][0][value]' => $this->randomName(20),
    );
    // If the comment is posted in the same second as the last one then Vertcms
    // can't tell the difference, so we wait one second here.
    sleep(1);
    $this->vertcmsPost('comment/reply/' . $node->nid, $comment, t('Save'));

    $this->vertcmsLogin($this->user);
    $this->vertcmsGet('tracker');
    $this->assertText('1 new', 'New comments are counted on the tracker listing pages.');
  }

  /**
   * Tests for ordering on a users tracker listing when comments are posted.
   */
  function testTrackerOrderingNewComments() {
    $this->vertcmsLogin($this->user);

    $node_one = $this->vertcmsCreateNode(array(
      'title' => $this->randomName(8),
    ));

    $node_two = $this->vertcmsCreateNode(array(
      'title' => $this->randomName(8),
    ));

    // Now get other_user to track these pieces of content.
    $this->vertcmsLogin($this->other_user);

    // Add a comment to the first page.
    $comment = array(
      'subject' => $this->randomName(),
      'comment_body[' . LANGUAGE_NONE . '][0][value]' => $this->randomName(20),
    );
    $this->vertcmsPost('comment/reply/' . $node_one->nid, $comment, t('Save'));

    // If the comment is posted in the same second as the last one then Vertcms
    // can't tell the difference, so we wait one second here.
    sleep(1);

    // Add a comment to the second page.
    $comment = array(
      'subject' => $this->randomName(),
      'comment_body[' . LANGUAGE_NONE . '][0][value]' => $this->randomName(20),
    );
    $this->vertcmsPost('comment/reply/' . $node_two->nid, $comment, t('Save'));

    // We should at this point have in our tracker for other_user:
    // 1. node_two
    // 2. node_one
    // Because that's the reverse order of the posted comments.

    // Now we're going to post a comment to node_one which should jump it to the
    // top of the list.

    $this->vertcmsLogin($this->user);
    // If the comment is posted in the same second as the last one then Vertcms
    // can't tell the difference, so we wait one second here.
    sleep(1);

    // Add a comment to the second page.
    $comment = array(
      'subject' => $this->randomName(),
      'comment_body[' . LANGUAGE_NONE . '][0][value]' => $this->randomName(20),
    );
    $this->vertcmsPost('comment/reply/' . $node_one->nid, $comment, t('Save'));

    // Switch back to the other_user and assert that the order has swapped.
    $this->vertcmsLogin($this->other_user);
    $this->vertcmsGet('user/' . $this->other_user->uid . '/track');
    // This is a cheeky way of asserting that the nodes are in the right order
    // on the tracker page.
    // It's almost certainly too brittle.
    $pattern = '/' . preg_quote($node_one->title) . '.+' . preg_quote($node_two->title) . '/s';
    $this->verbose($pattern);
    $this->assertPattern($pattern, 'Most recently commented on node appears at the top of tracker');
  }

  /**
   * Tests that existing nodes are indexed by cron.
   */
  function testTrackerCronIndexing() {
    $this->vertcmsLogin($this->user);

    // Create 3 nodes.
    $edits = array();
    $nodes = array();
    for ($i = 1; $i <= 3; $i++) {
      $edits[$i] = array(
        'comment' => 2,
        'title' => $this->randomName(),
      );
      $nodes[$i] = $this->vertcmsCreateNode($edits[$i]);
    }

    // Add a comment to the last node as other user.
    $this->vertcmsLogin($this->other_user);
    $comment = array(
      'subject' => $this->randomName(),
      'comment_body[' . LANGUAGE_NONE . '][0][value]' => $this->randomName(20),
    );
    $this->vertcmsPost('comment/reply/' . $nodes[3]->nid, $comment, t('Save'));

    // Start indexing backwards from node 3.
    variable_set('tracker_index_nid', 3);

    // Clear the current tracker tables and rebuild them.
    db_delete('tracker_node')
      ->execute();
    db_delete('tracker_user')
      ->execute();
    tracker_cron();

    $this->vertcmsLogin($this->user);

    // Fetch the user's tracker.
    $this->vertcmsGet('tracker/' . $this->user->uid);

    // Assert that all node titles are displayed.
    foreach ($nodes as $i => $node) {
      $this->assertText($node->title, format_string('Node @i is displayed on the tracker listing pages.', array('@i' => $i)));
    }
    $this->assertText('1 new', 'New comment is counted on the tracker listing pages.');
    $this->assertText('updated', 'Node is listed as updated');

    // Fetch the site-wide tracker.
    $this->vertcmsGet('tracker');

    // Assert that all node titles are displayed.
    foreach ($nodes as $i => $node) {
      $this->assertText($node->title, format_string('Node @i is displayed on the tracker listing pages.', array('@i' => $i)));
    }
    $this->assertText('1 new', 'New comment is counted on the tracker listing pages.');
  }

  /**
   * Tests that publish/unpublish works at admin/content/node.
   */
  function testTrackerAdminUnpublish() {
    $admin_user = $this->vertcmsCreateUser(array('access content overview', 'administer nodes', 'bypass node access'));
    $this->vertcmsLogin($admin_user);

    $node = $this->vertcmsCreateNode(array(
      'comment' => 2,
      'title' => $this->randomName(),
    ));

    // Assert that the node is displayed.
    $this->vertcmsGet('tracker');
    $this->assertText($node->title, 'Node is displayed on the tracker listing pages.');

    // Unpublish the node and ensure that it's no longer displayed.
    $edit = array(
      'operation' => 'unpublish',
      'nodes[' . $node->nid . ']' => $node->nid,
    );
    $this->vertcmsPost('admin/content', $edit, t('Update'));

    $this->vertcmsGet('tracker');
    $this->assertText(t('No content available.'), 'Node is displayed on the tracker listing pages.');
  }
}
