<?php

/**
 * @file
 * Handles incoming requests to fire off regularly-scheduled tasks (cron jobs).
 */

/**
 * Root directory of Vertcms installation.
 */
define('VERTCMS_ROOT', getcwd());

include_once VERTCMS_ROOT . '/zengine/includes/bootstrap.inc';
vertcms_bootstrap(VERTCMS_BOOTSTRAP_FULL);

if (!isset($_GET['cron_key']) || variable_get('cron_key', 'vertcms') != $_GET['cron_key']) {
  watchdog('cron', 'Cron could not run because an invalid key was used.', array(), WATCHDOG_NOTICE);
  vertcms_access_denied();
}
elseif (variable_get('maintenance_mode', 0)) {
  watchdog('cron', 'Cron could not run because the site is in maintenance mode.', array(), WATCHDOG_NOTICE);
  vertcms_access_denied();
}
else {
  vertcms_cron_run();
}
