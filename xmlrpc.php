<?php

/**
 * @file
 * PHP page for handling incoming XML-RPC requests from clients.
 */

/**
 * Root directory of Vertcms installation.
 */
define('VERTCMS_ROOT', getcwd());

include_once VERTCMS_ROOT . '/zengine/includes/bootstrap.inc';
vertcms_bootstrap(VERTCMS_BOOTSTRAP_FULL);
include_once VERTCMS_ROOT . '/zengine/includes/xmlrpc.inc';
include_once VERTCMS_ROOT . '/zengine/includes/xmlrpcs.inc';

xmlrpc_server(module_invoke_all('xmlrpc'));
